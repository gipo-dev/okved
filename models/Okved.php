<?php

namespace Models;

/**
 * @property int id
 * @property string code
 * @property string name
 * @property string description
 * @property string parent_id
 */
class Okved extends \Illuminate\Database\Eloquent\Model
{
    protected $guarded = [];

    public function parent()
    {
        return $this->belongsTo(Okved::class, 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(Okved::class, 'parent_id');
    }
}
<?php

namespace Services\LegalForms;

abstract class AbstractLegalForm
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function generateDocuments()
    {
        return $this->getDocuments();
    }

    /**
     * @return array
     */
    abstract protected function getDocuments();
}

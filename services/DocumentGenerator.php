<?php

namespace Services;

use Illuminate\Support\Carbon;
use PhpOffice\PhpWord\TemplateProcessor;
use Services\LegalForms\AbstractLegalForm;
use Services\LegalForms\AnoLegalForm;
use Services\LegalForms\AssocLegalForm;
use Services\LegalForms\FondLegalForm;
use Services\LegalForms\OoLegalForm;

require_once 'PHPWord.php';

define('WORD_SOURCE_FOLDER', dirname(__FILE__) . '/../word/templates/nko/');
define('WORD_RESULT_FOLDER', dirname(__FILE__) . '/../word/result/');

class DocumentGenerator
{
    public function __construct()
    {
        setlocale(LC_ALL, 'ru' . '.utf-8', 'ru_RU' . '.utf-8', 'ru', 'ru_RU');
    }

    /**
     * @var string[]
     */
    private $legalForms = [
        'ano' => AnoLegalForm::class,
        'oo' => OoLegalForm::class,
        'f' => FondLegalForm::class,
        'assoc' => AssocLegalForm::class,
    ];

    /**
     * @param array $data
     * @return array
     */
    public function getLegalFormDocuments($data)
    {
        $data['result_folder'] = Carbon::now()->timestamp;
        $documents = [];
        /** @var AbstractLegalForm $legalForm */
        $legalForm = new $this->legalForms[$data['company']['form']]($data);
        foreach ($legalForm->generateDocuments() as $name => $source) {

            if (in_array($data['company']['form'], ['f', 'oo']) && !($data['company']['address_after_registration'] ?? false) && $name == 'Образец гарантийного письма от юрлица')
                continue;

            if (!str_ends_with($source, '.docx'))
                $documents[$name] = $source;
            else
                $documents[$name] = $this->generateDocument($source, $name, $data);
        }
        return $documents;
    }

    public function generateDocument($source, $name, $data)
    {
        if (!is_dir(WORD_RESULT_FOLDER . '/' . $data['result_folder'])) {
            mkdir(WORD_RESULT_FOLDER . '/' . $data['result_folder']);
        }

        $resultPath = $data['result_folder'] . '/' . $name . '.docx';
        $template = new TemplateProcessor(WORD_SOURCE_FOLDER . $source);

        $this->prepareVariables($data);
        $this->setCompanyFields($template, $data, $data);
        $this->setPersons($template, $data);
        $this->setBossFields($template, $data);
        $this->setFormFields($template, $data);

        ob_clean();
        $template->saveAs(WORD_RESULT_FOLDER . $resultPath);

        return $resultPath;
    }

    /**
     * @param TemplateProcessor $template
     * @param array $data
     * @return void
     */
    private function setCompanyFields(&$template, $data, &$_data)
    {
        $ao = explode(PHP_EOL, $data['company']['activity_object']);
        $activity_objects = [];

        foreach ($ao as $i => $object) {
            $_object = '2.2.' . ($i + 1) . '. ' . trim($object);
            if (($i + 1) == count($ao))
                $_object .= ';';
            else
                $_object .= ',';
            $activity_objects[] = trim($_object);
        }

        $data['company']['activity_objects'] = implode('<w:br/>⠀       ', $activity_objects);

        $data['company']['date_created_format'] = Carbon::parse(['company']['date_created'])->formatLocalized('«%d» %B %Y г');
        $data['company']['date_created_y'] = Carbon::parse(['company']['date_created'])->format('Y');

        if (mb_strlen($data['address']['city']) > 0) {
            $data['company']['city'] = trim($data['address']['city']);
            $data['company']['fcity'] = $data['address']['city_type_full'] . ' ' . $data['address']['city'];
        } else {
            $data['company']['city'] = trim($data['address']['settlement']);
            $data['company']['fcity'] = $data['address']['settlement_type_full'] . ' ' . $data['address']['settlement'];
            $_data['address']['city'] = $data['address']['settlement'];
            $_data['address']['city_type'] = $data['address']['settlement_type'];
            $_data['address']['city_type_full'] = $data['address']['settlement_type_full'];
        }
        $_data['address']['street_long'] = mb_substr($data['address']['street'], 19, 100);
        $_data['address']['city_long'] = mb_substr($data['address']['city'], 19, 100);

        if (isset($data['company']['full_name_en']) && trim($data['company']['full_name_en']) != '')
            $data['company']['full_name_en'] = '<w:br/>	Наименование на иностранном языке: ' . $data['company']['full_name_en'] . '.';
        else
            $data['company']['full_name_en'] = '';

        if (isset($data['company']['short_name']) && trim($data['company']['short_name']) != '') {
            $data['company']['short_name'] = '<w:br/>	Сокращенное наименование Организации: ' . $data['company']['short_name'] . '.';
        } else
            $data['company']['short_name'] = '';

        foreach ($data['company'] as $key => $value) {
            $template->setValue("company:$key", $value);
        }
    }

    /**
     * @param TemplateProcessor $template
     * @param array $data
     * @return void
     */
    private function setBossFields(&$template, &$data)
    {
        $boss = [
            ['Директор', 'Директором', 'Директора', 'Директору'],
            ['Генеральный директор', 'Генеральным директором', 'Генерального директора', 'Генеральному директору'],
            ['Президент', 'Президентом', 'Президента', 'Президенту'],
            ['Руководитель', 'Руководителем', 'Руководителя', 'Руководителю'],
            ['Исполнительный директор', 'Исполнительным директором', 'Исполнительного директора', 'Исполнительному директору'],
            ['Председатель', 'Председателем', 'Председателя', 'Председателю'],
        ];

        if (!isset($data['boss']['type']))
            $data['boss']['type'] = 0;

        foreach ($boss[$data['boss']['type']] as $i => $type) {
            $data['boss']['type_' . ($i + 1)] = $type;
        }

        $data['boss']['passport_date_format'] = Carbon::parse($data['boss']['passport_date'])
            ->format('dmY');

        $data['boss']['birth_date_format'] = Carbon::parse($data['boss']['birth_date'])
            ->format('dmY');

        $data['boss']['passport_date_format_f'] = Carbon::parse($data['boss']['passport_date'])
            ->format('d.m.Y');

        $data['boss']['birth_date_format_f'] = Carbon::parse($data['boss']['birth_date'])
            ->format('d.m.Y');

        $data['boss']['birth_date_y'] = Carbon::parse($data['boss']['birth_date'])
            ->format('Y');
        $data['boss']['birth_date_format'] = Carbon::parse($data['boss']['birth_date'])
            ->format('dmY');
        $data['boss']['birth_date_format_d'] = Carbon::parse($data['boss']['birth_date'])
            ->format('d');
        $data['boss']['birth_date_format_m'] = Carbon::parse($data['boss']['birth_date'])
            ->format('m');
        $data['boss']['birth_date_format_y'] = Carbon::parse($data['boss']['birth_date'])
            ->format('Y');

        foreach ($data['boss'] as $key => $value) {
            $template->setValue("director:$key", $value);
        }
    }

    /**
     * @param TemplateProcessor $template
     * @param array $data
     * @return void
     */
    public function setPersons(&$template, $data)
    {
        foreach (['person_or' => 'person', 'soviet' => 'soviet', 'popsoviet' => 'popsoviet', 'revcom' => 'revcom'] as $key => $skey) {
            $passport_datas = [];
            foreach ($data[$key] as &$person) {
                $person['passport_date_format'] = Carbon::parse($person['birth_date'])->format('d.m.Y');
                $passport_datas[] = $person['passport_data'] = "{$person['last_name']} {$person['name']} {$person['middle_name']}, паспорт серия {$person['passport_serial']} № {$person['passport_number']}, код подразделения " . ($person['passport_code_def'] ?? $person['passport_code']) . ", выдан {$person['passport_issued']}, {$person['passport_date_format']} года.";
                $person['fio'] = $person['last_name'] . ' ' . mb_substr($person['name'], 0, 1) . '. ' . mb_substr($person['middle_name'], 0, 1) . '.';
            }

            $template->setValue("company:{$skey}s_passports", implode('<w:br/>	', $passport_datas));
            $template->setValue("{$skey}s:count", $data["{$key}s_count"]);
        }

        foreach ($data['person_or'][0] ?? [] as $key => $value) {
            $template->setValue("fperson:$key", $value);
        }

        if (count($data['person_or']) > 1) {
            $chairman = $data['person_or'][$data['chairman']];
            $secretary = $data['person_or'][$data['secretary']];
        } else {
            $chairman = $data['boss'];
            $secretary = $data['boss'];
        }

        if ($chairman != null) {
            foreach ($chairman as $key => $value) {
                $template->setValue("chairman:$key", $value);
            }
        }

        if ($secretary != null) {
            foreach ($secretary as $key => $value) {
                $template->setValue("secretary:$key", $value);
            }
        }
    }

    /**
     * @param TemplateProcessor $template
     * @param array $data
     * @return void
     */
    public function setFormFields(&$template, $data)
    {
        $formGenerator = new FormGenerator();

        if (!in_array('mode:cells', $template->getVariables()))
            return;

        $template->cloneBlock('BLOCKPERSON', count($data['person']), true, true);
        $template->cloneBlock('BLOCKJUR', count($data['jurs']), true, true);

        $variables = $template->getVariableCount();

        $template->setValue('mode:cells', '');

        $page = 1;
        foreach ($variables as $variable => $count) {
            if (strpos($variable, ',page') > 0 || strpos($variable, ',spage') > 0) {
                $k1 = explode('#', $variable);
                if (count($k1) > 1)
                    $k1 = explode(':', $k1[1])[0];
                else
                    $k1 = false;
                $k2 = explode(':', $variable)[1];
                if ($k1) {
                    $k1 -= 1;
                    if (!isset($data['spage'][$k1]))
                        $data['spage'][$k1] = [];
                    $data['spage'][$k1][$k2] = str_pad($page++, 3, '0', STR_PAD_LEFT);
                } else
                    $data['page'][$k2] = str_pad($page++, 3, '0', STR_PAD_LEFT);
            }
        }

        foreach ($variables as $variable => $count) {
            if ($variable == 'mode:cells')
                continue;

            for ($i = 0; $i < $count; $i++) {
                $cells = $formGenerator->getCellFromVariable($variable, $data);
                $template->setComplexBlock($variable, $cells);
            }

        }
    }

    private function prepareVariables(&$data)
    {
        if ($data['address']['city_type'] == 'г') {
            $data['address']['city_code'] = 2;
            $data['address']['city_code_naming'] = 'городской округ';
        } else {
            $data['address']['city_code'] = 3;
            $data['address']['city_code_naming'] = 'муниципальный район';
        }

        $data['persons_count'] = count($data['person']);

        if (count($data['revcom'] ?? []) > 1)
            $data['company']['revcom_title'] = 'Избрать членами Ревозионной комиссии';
        else
            $data['company']['revcom_title'] = 'Избрать на должность Ревизора следующее лицо';

        $_persons = [];
        $_jurs = [];
        foreach ($data['person'] as $index => $person) {
            if ($person['type'] == 1)
                $_persons[$index] = $person;
            else
                $_jurs[$index] = $person;
        }
        $data['person_or'] = $data['person'];
        $data['person'] = $_persons;
        $data['jurs'] = $_jurs;

        foreach ($data['person'] as $index => &$person) {
            $this->setupPersonFields($person);
            $person['percent'] = intval(100 / count($data['person']));
            $person['sum'] = intval(10000 / count($data['person']));
        }

        $data['declarant'] = $data['person'][$data['requester']];

        $data['boss'] = $data['boss'][0];
        $data['boss']['passport_code_def'] = $data['boss']['passport_code'];
        $data['boss']['passport_code'] = str_replace('-', '', $data['boss']['passport_code']);
        $data['boss']['fio'] = $data['boss']['last_name'] . ' ' . mb_substr($data['boss']['name'], 0, 1)
            . '. ' . mb_substr($data['boss']['middle_name'], 0, 1) . '.';

        if (count($data['person_or']) > 1) {
        } else {
            $data['chairman'] = $data['boss'];
        }

        $data['boss']['pass'] = $data['boss']['passport_serial'] . ' ' . $data['boss']['passport_number'];

        foreach ($data['okved'] as &$okved) {
            $okved = str_replace('.', '', $okved);
        }

        $data['main'] = str_replace('.', '', $data['main']);

        $data['person_orig'] = $data['person'];
        $data['jurs_orig'] = $data['jurs'];
        $data['person'] = array_values($data['person']);
        $data['jurs'] = array_values($data['jurs']);

        $data['person_ors_count'] = count($data['person_or']);
        $data['soviets_count'] = count($data['soviet'] ?? []);
        $data['popsoviets_count'] = count($data['popsoviet'] ?? []);
        $data['revcoms_count'] = count($data['revcom'] ?? []);
    }

    private function setupPersonFields(&$person)
    {
        $person['passport_code_def'] = $person['passport_code'];
        $person['passport_code'] = str_replace('-', '', $person['passport_code']);
        $person['passport_code2'] = $person['passport_code'];
        $person['pass'] = $person['passport_serial'] . ' ' . $person['passport_number'];
        $person['passport_date_format'] = Carbon::parse($person['passport_date'])
            ->format('dmY');
        $person['birth_date_y'] = Carbon::parse($person['birth_date'])
            ->format('Y');
        $person['birth_date_format'] = Carbon::parse($person['birth_date'])
            ->format('dmY');
        $person['birth_date_format_d'] = Carbon::parse($person['birth_date'])
            ->format('d');
        $person['birth_date_format_m'] = Carbon::parse($person['birth_date'])
            ->format('m');
        $person['birth_date_format_y'] = Carbon::parse($person['birth_date'])
            ->format('Y');
        $person['fio'] = $person['last_name'] . ' ' . mb_substr($person['name'], 0, 1)
            . '. ' . mb_substr($person['middle_name'], 0, 1) . '.';
    }
}

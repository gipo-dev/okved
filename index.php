<?php

ini_set('error_reporting', E_ERROR);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

use Models\Okved;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Services\DocumentGenerator;
use Slim\Factory\AppFactory;
use Twig\Loader\FilesystemLoader;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/services/database.php';

$loader = new FilesystemLoader('templates');
$view = new \Twig\Environment($loader);

$app = AppFactory::create();

$app->get('/', function (Request $request, Response $response, $args) use ($view) {
    $response->getBody()->write(
        $view->render('pages/form.twig')
    );
    return $response;
});

$app->get('/find', function (Request $request, Response $response, $args) use ($view) {
    $text = $request->getQueryParams()['text'];
    $result = Okved::with(['parent', 'childs'])
        ->whereNotNull('parent_id')
        ->where(function ($q) use ($text) {
            $q->orWhere('code', 'like', "%$text%")
                ->orWhere('name', 'like', "%$text%");
//                ->orWhere('description', 'like', "%$text%"); //включает поиск по полю описания
        })->limit(100)->get(); //limit задает макс кол-во записей

    $result = $result->filter(function ($item) use ($result) {
        return !$item->parent || $result->where('code', $item->parent->code)->count() == 0;
    });

    $response->getBody()->write(json_encode($result->toArray()));
    return $response;
});

$app->get('/parse', function (Request $request, Response $response, $args) use ($view) {
    (new \Services\OkvedParser())->update();
    $response->getBody()->write('ok');
    return $response;
});

$app->post('/generate', function (Request $request, Response $response, $args) use ($view) {
    $properties = $request->getParsedBody();
    $documents = (new DocumentGenerator())->getLegalFormDocuments($properties);

    $response->getBody()->write(
        json_encode($documents)
    );
    return $response;
});

$app->run();

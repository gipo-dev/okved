<?php

namespace Services;

use DOMDocument;
use Models\Okved;

class OkvedParser
{
    const FILEPATH = 'okveds.json';

    public function update()
    {
        $content = file_get_contents('https://okvd-2.ru/skachat-kodyi-okved.html');
        $doc = new DOMDocument();
        $doc->loadHTML($content);
        Okved::truncate();

        $tables = $doc->getElementsByTagName('table');
        foreach ($tables as $i => $table) {

            if ($i == 0)
                continue;

            $rows = $table->getElementsByTagName('tr');

            foreach ($rows as $row) {

                $data = explode(PHP_EOL, trim($row->textContent));
                if (trim($data[0]) == 'Расшифровка')
                    continue;

                $code = explode('.', trim($data[0]));
                $currentLevel = null;
                $parent = null;

                for ($i = 0; $i < count($code); $i++) {
                    $parent = $currentLevel;
                    $currentLevel = Okved::firstOrCreate([
                        'code' => implode('.', array_slice($code, 0, $i + 1)),
                    ]);
                }

                $currentLevel->update([
                    'name' => trim($data[1]),
                    'description' => trim(implode(PHP_EOL, array_slice($data, 2)) ?? ''),
                    'parent_id' => $parent->id ?? null,
                ]);
            }
        }
    }

}
<!DOCTYPE html>
<html lang="ru">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@21.12.0/dist/css/suggestions.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@21.12.0/dist/js/jquery.suggestions.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script type="text/javascript" src="/boot/bootstrap.min.js"></script>
<link rel="stylesheet" href="/boot/bootstrap.min.css" type="text/css">

<h3>Демо отправки в документ окведов</h3>
<p>Переменные в документ передаются через файл load.php.
    Пример: $document->setValue('okved', $_POST['okved2']);
    Шаблон документа называется okved.docx. По тз надо будет заменить его на форму и сделать ввод окведов поциферно в
    клетки. Чтобы добавить туда переменную, наименование например, добавляем в load.php строку
    $document->setValue('name', $_POST['name']); Далее нужно скачать файл okved.docx и добавить туда переменную ${name},
    сохранить документ и закачать обратно на хостинг.
    Сохранять файл надо именно в формате docx. Для скачивания библиотекой создается документ okved-print.docx
</p>
<form id="form-demo">
    <label style="display: block;margin-bottom: 10px;">Оквэд</label>
    <input id="okved2" type="text" name="okved2" value="<?php if (isset($_POST['okved2'])) echo $_POST['okved2']; ?>"
           placeholder="Оквэд">
    <br>
    <button type="submit" style="padding: 8px 16px;width:300px">отправить</button>
    <br>
    <br>
    Выбрано:
    <div id=message></div>
</form>
<a href="okved-print.docx" download="">скачать документы</a>

<!--шаги-->

<div class="steps">
    <div class=col-sm-1 id="step1">
        1. Наименование
    </div>
    <div class=col-sm-1 id="step2">
        2. Адрес (местонахождение и юридический адрес)
    </div>
    <div class=col-sm-1 id="step3">
        3. Учредители
    </div>
    <div class=col-sm-1 id="step4">
        4. Цель и предмет деятельности
    </div>
    <div class=col-sm-1 id="step5">
        5. Исполнительный орган
    </div>
    <div class=col-sm-1 id="step6">
        6. ОКВЭД
    </div>
</div>

<!--шаг1: Наименование-->


<div class=col-sm-12 id=form1>
    <p style="border: 1px solid #03a9f4;padding: 20px;margin: 0;border-left: 6px solid #03a9f4;"> «По законодательству
        РФ название НКО должно содержать ссылку на правовую модель и вид деятельности организации и собственно название.
        Главным является соответствие формы НКО и направления деятельности. Правильное указание этих данных влияет на
        положительное решение о регистрации НКО. Например, Автономная некоммерческая организация центр поддержки
        гражданских инициатив «ЩИТ». </p>
    <form method="POST" id="forms">

        <div class=col-sm-6>
            <label style="display: block; margin-bottom: 10px;">Полное наименование</label>
            <input id="name" type="text" name="name" value="<?php if (isset($_POST['name'])) echo $_POST['name']; ?>"
                   placeholder="Введите полное наименование">

            <label style="display: block; margin-bottom: 10px;">Сокращенное наименование</label>
            <input id="names" type="text" name="name-short"
                   value="<?php if (isset($_POST['name-short'])) echo $_POST['name-short']; ?>"
                   placeholder="Введите сокращенное наименование">

            <label style="display: block; margin-bottom: 10px;">Наименование на англ. языке</label>
            <input id="name" type="text" name="enname"
                   value="<?php if (isset($_POST['enname'])) echo $_POST['enname']; ?>"
                   placeholder="Введите наименование" required="">

            <label style="display: block; margin-bottom: 10px;">Сокращенное наименование на англ. языке</label>
            <input id="name" type="text" name="ennames"
                   value="<?php if (isset($_POST['ennames'])) echo $_POST['ennames']; ?>"
                   placeholder="Введите наименование" required="">

            <label style="display: block;margin-bottom: 10px;">Дата создания документов</label>
            <input id="date" type="text" name="date" value="<?php if (isset($_POST['date'])) echo $_POST['date']; ?>"
                   placeholder="Введите дату" required="">
        </div>

        <div class=col-sm-6>
            <label style="display: block;margin-bottom: 10px;">E-mail для внесения в ЕГРЮЛ</label>
            <input id="mail" type="mail" name="mail" value="<?php if (isset($_POST['mail'])) echo $_POST['mail']; ?>"
                   placeholder="Ваш email" required="">
            <label style="display: block;margin-bottom: 10px;">Номер контактного телефона заявителя</label>
            <input type="phone" id="phone" name="phone"
                   value="<?php if (isset($_POST['phone'])) echo $_POST['phone']; ?>" placeholder="Ваша телефон"
                   required="">
            <label style="display: block;margin-bottom: 10px;">Время заседания высшего органа управления </label>
            <input id="time" type="text" name="time" placeholder="Время заседания" required="">
        </div>

        <div class=col-sm-12>
            <label for="paper">Поставьте отметку, если вам необходимо получение документов на бумажном носителе</label>
            <input type=checkbox name="paper" id="paper"
                   style="float: left;width: 15px;height: 15px;margin-right: 10px;"><br>
            <button type="submit" class="send" style="padding: 8px 16px;" id="steps1">далее</button>
    </form>
</div>
</div>
</div>


<!--шаг1: Наименование-->

<!--шаг2: Адрес-->

<div class=col-sm-12 id=form2>
    <p style="border: 1px solid #03a9f4; padding: 20px;margin: 0;border-left: 6px solid #03a9f4;">
        Адрес регистрации АНО указывается полностью (например: 680000, Российская Федерация, Хабаровский край, городской
        округ город Хабаровск, город Хабаровск, улица Калараша, дом 11а, кв. 1).
        Предоставляется домашний адрес одного из учредителей при условии, что данный адрес принадлежит учредителю на
        праве собственности. Так же возможно указывать адрес помещения принадлежащее учредителям(ю) на праве
        безвозмездного пользования/аренды. Если учредитель владеет только долей, необходимо получить письменное согласие
        других собственников.
        ВАЖНО: Адрес регистрации — это то место, куда будет направляться корреспонденция, а также место, которое будут
        посещать представители налогового органа с целью проведения контрольных проверок, поэтому к данному вопросу
        стоит относится ответственно.
    </p>
    <form action="/" method="POST" id="forms2">
        <div class=col-sm-12>
            <h3>Адрес</h3>
            <label style="display: block;margin-bottom: 10px;">Юридический адрес организации</label>
            <input id="address" type="text" name="address" value="" placeholder="Юр адрес" data-validate=""><br><br>
            <button type="submit" class="send" style="padding: 8px 16px;" id="steps2">далее</button>
        </div>
</div>


<!--api dadata-->

<script>
    $("#okved2").suggestions({
        token: "8fbd51a617b396af94a794d683ec99a9d83364c0",
        type: "okved2",
        count: 15,
        onSelect: function (suggestion) {
            $("#okved2").val(suggestion.data.idx + suggestion.data.name);
        }
    });
</script>

<script>
    $("#address").suggestions({
        token: "8fbd51a617b396af94a794d683ec99a9d83364c0",
        type: "ADDRESS",
        // Вызывается, когда пользователь выбирает одну из подсказок
        onSelect: function (suggestion) {
            console.log(suggestion);
        }
    });
</script>


<!--Аякс-->

<script>
    jQuery(document).ready(function ($) {
        $('#form2').hide();
        $('#steps1, #step2').click(function () {
            $('#form2').show();
            $('#form1').hide();
        })
        $('#phone').inputmask("+9-(999)-999-99-99");
        $('#time').inputmask("99:99");
        $('#date').inputmask("99.99.9999");
        $('#mail').inputmask("email");
        $("#form-demo").on("submit", function () {
            $.ajax({
                url: 'http://okved.avs29rmf.beget.tech/load.php',
                method: 'post',
                dataType: 'html',
                data: $(this).serialize(),
                success: function (data) {
                    $('#message').html(data);
                }
            });
            event.preventDefault();
        });
    });
</script>


<style>

    a {
        padding: 6px 10px;
        border: 2px solid;
        display: inline-block;
        margin: 20px 0px;
        text-decoration: none;
        font-size: 20px;
        font-weight: 600;
    }

    input {
        max-width: 400px !important;
        height: 40px
    }

    .send {
        padding: 8px 16px;
        background: #03a9f4;
        max-width: 300px;
        display: block;
        margin: 0 auto;
        margin-top: 30px;
        color: white;
        font-size: 20px;
        font-weight: 600;
    }

    button, input, select, textarea {
        width: 100%;
        background: #e8e6e6;
        border-radius: 5px;
        border: 1px solid #b2a4a4;
        padding: 15px;
    }

    form {
        margin-top: 20px;
    }


    /*#step2{pointer-events:none}*/

    .line {
        display: block;
        position: relative;
        margin-bottom: 15px;
    }

    .butn3, .butn4, .butn5, .butn6, .butn-step1, .butn-step2, .butn-step3 {
        display: flex;
        padding: 13px 60px;
        background: #03a9f4;
        color: white;
        font-weight: 600;
        max-width: 200px;
        flex-direction: row;
        align-items: stretch;
        justify-content: center;
        margin-bottom: 15px;
        min-width: 250px;
        border-radius: 5px;
        border-bottom: 4px solid #0c4c69;
        box-shadow: 2px 4px 9px #211f1f61;


    }

    #form1, #form2 {
        position: absolute;
        margin: 0 auto;
        left: 0;
        right: 0;
    }

    .buttons, .buttons2, #form1, #form2 {
        display: flex;
        min-height: 700px;
        height: auto;
        /* border: 1px solid; */
        width: 800px;
        margin: 0 auto;
        align-items: center;
        justify-content: center;
        box-shadow: 1px 1px 15px 1px #352e2e61;
        border-radius: 12px;
        margin-top: 50px;
        flex-direction: column;
        align-items: center;

    }

    .col-sm-1 {
        border: 1px solid #03a9f4;
        min-height: 70px;
        font-size: 12px;
        border-bottom: 5px solid #03a9f4;
        display: flex;
        align-items: center;
        justify-content: center;
        text-align: center;
        font-weight: 600;
    }

    .col-sm-2 {
        text-align: center;
        font-weight: 600;
        border-style: solid;
        border-width: 1px 1px 1px 1px;
        margin: 3px;
        min-height: 190px;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .butn, .butn2 {
        border: 1px solid;
        display: inline-block;
        padding: 5px;
        position: absolute;
        top: 140px;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: 600;
        padding: 10px 20px;
    }

    .steps {
        display: flex;
        align-items: center;
        justify-content: center;

    }

</style>

<?php

namespace Services;

use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\SimpleType\Border;

class FormGenerator
{
    static $page = 1;

    /**
     * @param $variable
     * @param $data
     * @return \PhpOffice\PhpWord\Element\Table
     */
    public function getCellFromVariable($variable, $data)
    {
        if (strpos($variable, '#') !== false) {
            preg_match('/\#(\d+)/U', $variable, $index);
            $variable = preg_replace('/\#(\d+)/U', ':' . ($index[1] - 1), $variable);
        }

        $params = explode(',', $variable);

        if (count($params) == 2 && strpos($params[1], 'mask:') !== false) {
            return $this->generateCellsByMask(
                $this->getVariableFromData($params[0], $data)
                , $params[1]
            );
        } else if (count($params) > 3) {
            return $this->generateCells(
                $this->getVariableFromData($params[5], $data)
                , $params[0], $params[1], $params[2], $params[3], $params[4]
            );
        } else {
            return $this->generateCells(
                $this->getVariableFromData($params[2], $data)
                , $params[0], $params[1]
            );
        }

    }

    /**
     * @param $variable
     * @param $data
     * @return mixed
     */
    private function getVariableFromData($variable, $data)
    {
        $levels = explode(':', $variable);
        $currentLevel = $data;

        for ($i = 0; $i < count($levels); $i++) {
            $currentLevel = $currentLevel[$levels[$i]];
        }

        return $currentLevel;
    }

    /**
     * @param $text
     * @param $cols
     * @param $rows
     * @param $by
     * @param $space
     * @param $delimeter
     * @return \PhpOffice\PhpWord\Element\Table
     */
    public function generateCells($text, $cols = 40, $rows = 6, $by = 1, $space = 0, $delimeter = '')
    {
        $text = mb_strtoupper($text);

        if ($cols >= 40)
            $cellWidth = 11016 / $cols;
        else
            $cellWidth = 275.4;

        $table = new Table([
            'cellMarginLeft' => $cellWidth * .110909091,
            'cellMarginTop' => 0,
        ]);
        $_symbols = mb_str_split($text);
        $letterIndex = 0;
        $symbols = [];

        for ($i = 0; $i < $rows * $cols; $i++) {

            for ($j = 0; $j < $by; $j++) {
                $symbols[] = $_symbols[$letterIndex++];
            }

            for ($j = 0; $j < $space; $j++) {
                $symbols[] = '||';
            }

        }

        $letterIndex = 0;

        for ($i = 0; $i < $rows; $i++) {

            $row = $table->addRow($cellWidth * 1.236, ['exactHeight' => true]);
            $rowIndex = 0;

            if ($i + 1 < $rows) {
                $row2 = $table->addRow($cellWidth * .4182, ['exactHeight' => true]);
            }

            for ($j = 0; $j < $cols; $j++) {

                if ($symbols[$letterIndex] != '||') {
                    $cell = $row->addCell($cellWidth, [
                        'valign' => 'center',
                        'borderColor' => '000000',
                        'borderStyle' => Border::DOTTED,
                        'borderSize' => 3,
                    ]);
                    $cell->addText($symbols[$letterIndex] ?? '', [
                        'size' => 18,
                        'name' => 'Courier New',
                    ]);
                } else {
                    if ($j == 0) {
                        $cell = $row->addCell($cellWidth, [
                            'borderLeftColor' => '000000',
                            'borderLeftStyle' => Border::DOTTED,
                            'borderLeftSize' => 3,
                            'borderRightColor' => '000000',
                            'borderRightStyle' => Border::DOTTED,
                            'borderRightSize' => 3,
                        ]);
                    } else {
                        $cell = $row->addCell($cellWidth);
                    }
                    $cell->addText($delimeter, [
                        'size' => 18,
                        'name' => 'Courier New',
                    ]);
                }

                if ($i + 1 < $rows) {

                    if ($symbols[$letterIndex] != '||') {
                        $row2->addCell($cellWidth, [
                            'borderTopColor' => '000000',
                            'borderTopStyle' => Border::DOTTED,
                            'borderTopSize' => 3,
                            'borderBottomColor' => '000000',
                            'borderBottomStyle' => Border::DOTTED,
                            'borderBottomSize' => 3,
                        ]);
                    } else {
                        $row2->addCell($cellWidth);
                    }

                }

                $letterIndex++;
                $rowIndex++;
            }
        }

        return $table;
    }

    /**
     * @param $text
     * @param $mask
     * @return \PhpOffice\PhpWord\Element\Table
     */
    public function generateCellsByMask($text, $mask)
    {
        $cellWidth = 275.4;
        $text = mb_strtoupper($text);
        $mask = str_replace('mask:', '', $mask);
        $table = new Table(['cellMargin' => $cellWidth * .210909091]);
        $row = $table->addRow($cellWidth * 1.236, ['exactHeight' => true]);

        $symbols = mb_str_split($text);
        $symbolIndex = 0;

        $vals = [];

        foreach (mb_str_split($mask) as $point) {
            if ($point == '*') {
                $cell = $row->addCell($cellWidth, [
                    'borderLeftColor' => '000000',
                    'borderLeftStyle' => Border::DOTTED,
                    'borderLeftSize' => 3,
                    'borderRightColor' => '000000',
                    'borderRightStyle' => Border::DOTTED,
                    'borderRightSize' => 3,
                ]);
                $cell->addText($symbols[$symbolIndex], [
                    'size' => 18,
                    'name' => 'Courier New',
                ]);
                $vals[] = '(' . $symbols[$symbolIndex] . ')';
                $symbolIndex++;
            } else {
                $cell = $row->addCell($cellWidth);
                $cell->addText($point, [
                    'size' => 18,
                    'name' => 'Courier New',
                ]);
                $vals[] = '(' . $point . ')';
            }
        }

        return $table;
    }
}
